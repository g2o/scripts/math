const rad2Deg = 57.2958
const deg2Rad = 0.0174533

function validArgument(obj, ...)
{
	local test = false
	local objType = type(obj)

	foreach (arg in vargv)
	{
		switch (type(arg))
		{
			case "class":
				test = (obj instanceof arg)
				break
				
			default:
				test = (objType == arg)
				break
		}
		
		if (test)
			return true
	}
	
	return false
}

function min(a, b)
{
	if (a < b)
		return a
		
	return b
}

function max(a, b)
{
	if (a > b)
		return a
		
	return b
}

function round(number, dec_pos = 0)
{
  local to_int = pow(10, dec_pos)
  return floor(number * to_int + 0.5) / to_int
}

function sign(number)
{
	if (number < 0)
		return -1
		
	return 1
}

function clamp(value, min, max)
{
	if (value < min)
		return min
	
	if (value > max)
		return max
		
	return value
}
