local vecZero = null
local vecOne = null

// class

class Vector4
{
	static epsilon = 0.000001

	x = 0.0
	y = 0.0
	z = 0.0
    w = 0.0
	
	constructor (...)
	{
		switch (vargv.len())
		{
			// copy constructor
			
			case 1:
				if (!validArgument(vargv[0], Vector4, "table"))
					throw "parameter 1 has an invalid type '"+type(vargv[0])+"' ; expected: 'Vector4|table'"
			
				x = vargv[0].x
				y = vargv[0].y
				z = vargv[0].z
                w = vargv[0].w
			
				break
				
			// initial constructor
			
			case 4:			
				if (!validArgument(vargv[0], "integer", "float"))
					throw "parameter 1 has an invalid type '"+type(vargv[0])+"' ; expected: 'integer|float'"
					
				if (!validArgument(vargv[1], "integer", "float"))
					throw "parameter 2 has an invalid type '"+type(vargv[1])+"' ; expected: 'integer|float'"
					
				if (!validArgument(vargv[2], "integer", "float"))
					throw "parameter 3 has an invalid type '"+type(vargv[2])+"' ; expected: 'integer|float'"
			
                if (!validArgument(vargv[3], "integer", "float"))
					throw "parameter 4 has an invalid type '"+type(vargv[3])+"' ; expected: 'integer|float'"

				x = vargv[0].tofloat()
				y = vargv[1].tofloat()
				z = vargv[2].tofloat()
                w = vargv[3].tofloat()
			
				break
		}
	}
	
	// overloaded operators
	
	function _get(idx)
	{			
		switch (idx)
		{
			case 0: return x
			case 1: return y
			case 2: return z
            case 3: return w
			
			default: throw null
		}
	}
	
	function _set(idx, value)
	{			
		switch (idx)
		{
			case 0:
				x = value
				break
				
			case 1:
				y = value
				break
				
			case 2:
				z = value
				break

            case 3:
                w = value
                break
				
			default: 
				throw null
		}
	}
	
	function _add(arg)
	{
		local vec = Vector4(x, y, z, w)
	
		if (validArgument(arg, Vector4, "table"))
		{
			vec.x += arg.x
			vec.y += arg.y
			vec.z += arg.z
            vec.w += arg.w
		}
		else
		{
			vec.x += arg
			vec.y += arg
			vec.z += arg
            vec.w += arg
		}
		
		return vec
	}
	
	function _sub(arg)
	{		
		return this + -arg
	}
	
	function _unm()
	{		
		return Vector4(-x, -y, -z, -w)
	}
	
	function _mul(arg)
	{
		local vec = Vector4(x, y, z)
	
		if (validArgument(arg, Vector4, "table"))
		{
			vec.x *= arg.x
			vec.y *= arg.y
			vec.z *= arg.z
            vec.w *= arg.w
		}
		else
		{
			vec.x *= arg
			vec.y *= arg
			vec.z *= arg
            vec.w *= arg
		}
		
		return vec
	}
	
	function _div(arg)
	{
		if (validArgument(arg, Vector4, "table"))
			return this * Vector4(1.0 / arg.x, 1.0 / arg.y, 1.0 / arg.z, 1.0 / arg.w)
		else
			return this * (1.0 / arg)
	}
	
	// methods
	
	function add(arg)
	{
		if (validArgument(arg, Vector4, "table"))
		{
			x += arg.x
			y += arg.y
			z += arg.z
            w += arg.w
		}
		else
		{
			x += arg
			y += arg
			z += arg
            w += arg
		}
	}
	
	function sub(arg)
	{
		add(-arg)
	}
	
	function mul(arg)
	{
		if (validArgument(arg, Vector4, "table"))
		{
			x *= arg.x
			y *= arg.y
			z *= arg.z
            w *= arg.w
		}
		else
		{
			x *= arg
			y *= arg
			z *= arg
            w *= arg
		}
	}
	
	function div(arg)
	{
		if (validArgument(arg, Vector4, "table"))
		{
			x /= arg.x
			y /= arg.y
			z /= arg.z
            w /= arg.w
		}
		else
		{
			x /= arg
			y /= arg
			z /= arg
            w /= arg
		}
	}
	
	function set(x, y, z, w)
	{
		this.x = x
		this.y = y
		this.z = z
        this.w = w
	}
	
	function equals(vec)
	{
		if (!validArgument(vec, Vector4, "table"))
			throw "parameter 1 has an invalid type '"+type(vec)+"' ; expected: 'Vector4|table'"
	
		local diff_x = x - vec.x
        local diff_y = y - vec.y
        local diff_z = z - vec.z
        local diff_w = w - vec.w

        local sqrMagnitude = diff_x * diff_x + diff_y * diff_y + diff_z * diff_z + diff_w * diff_w

        return sqrMagnitude < epsilon * epsilon
	}
	
	function sqrLength()
	{
		return (x * x + y * y + z * z + w * w)
	}
	
	function length()
	{
		return sqrt(sqrLength())
	}
	
	function sqrMagnitude()
	{
		return sqrLength()
	}
	
	function magnitude()
	{
		return length()
	}
	
	function normalized()
	{
		local vec = Vector4(this)
		local magnitude = magnitude()

		if (magnitude != 0)
			vec.mul(1.0 / magnitude)
		else
			vec.x = vec.y = vec.z = vec.w = 0
		
		return vec
	}
	
	function tostring(pattern = "Vector4(%f, %f, %f, %f)")
	{
		return format(pattern, x, y, z, w)
	}
	
	// static methods

	static function zero() { return vecZero }
	static function one() { return vecOne }
	
	static function min(lhs, rhs)
	{
		if (!validArgument(lhs, Vector4, "table"))
			throw "parameter 1 has an invalid type '"+type(lhs)+"' ; expected: 'Vector4|table'"
			
		if (!validArgument(rhs, Vector4, "table"))
			throw "parameter 2 has an invalid type '"+type(rhs)+"' ; expected: 'Vector4|table'"
		
		return Vector4(min(lhs.x, rhs.x), min(lhs.y, rhs.y), min(lhs.z, rhs.z), min(lhs.w, rhs.w))
	}
	
	static function max(lhs, rhs)
	{
		if (!validArgument(lhs, Vector4, "table"))
			throw "parameter 1 has an invalid type '"+type(lhs)+"' ; expected: 'Vector4|table'"
			
		if (!validArgument(rhs, Vector4, "table"))
			throw "parameter 2 has an invalid type '"+type(rhs)+"' ; expected: 'Vector4|table'"
			
		return Vector4(max(lhs.x, rhs.x), max(lhs.y, rhs.y), max(lhs.z, rhs.z), max(lhs.w, rhs.w))
	}
	
	static function distance(a, b)
	{
		if (!validArgument(a, Vector4, "table"))
			throw "parameter 1 has an invalid type '"+type(a)+"' ; expected: 'Vector4|table'"
			
		if (!validArgument(b, Vector4, "table"))
			throw "parameter 2 has an invalid type '"+type(b)+"' ; expected: 'Vector4|table'"
	
		return (a - b).magnitude()
	}
	
	static function scale(a, b)
	{
		if (!validArgument(a, Vector4, "table"))
			throw "parameter 1 has an invalid type '"+type(a)+"' ; expected: 'Vector4|table'"
			
		if (!validArgument(b, Vector4, "table"))
			throw "parameter 2 has an invalid type '"+type(b)+"' ; expected: 'Vector4|table'"
		
		return a * b
	}
	
	static function moveTowards(current, target, maxDistanceDelta)
	{
		if (!validArgument(current, Vector4, "table"))
			throw "parameter 1 has an invalid type '"+type(current)+"' ; expected: 'Vector4|table'"
			
		if (!validArgument(target, Vector4, "table"))
			throw "parameter 2 has an invalid type '"+type(target)+"' ; expected: 'Vector4|table'"

		if (!validArgument(maxDistanceDelta, "integer", "float"))
			throw "parameter 3 has an invalid type '"+type(maxDistanceDelta)+"' ; expected: 'integer|float'"
		
		local distance = target - current
		local magnitude = distance.magnitude()
		
		if (magnitude <= maxDistanceDelta || magnitude == 0)
			return target
		
		return current + (distance / magnitude) * maxDistanceDelta
	}
	
	static function normalize(vec)
	{
		if (!validArgument(vec, Vector4, "table"))
			throw "parameter 1 has an invalid type '"+type(vec)+"' ; expected: 'Vector4|table'"
	
		local magnitude = vec.magnitude()

		if (magnitude != 0)
			vec.mul(1.0 / magnitude)
		else
			vec.x = vec.y = vec.z = vec.w = 0
			
		return vec
	}
	
	static function dot(lhs, rhs)
	{
		if (!validArgument(lhs, Vector4, "table"))
			throw "parameter 1 has an invalid type '"+type(lhs)+"' ; expected: 'Vector4|table'"
			
		if (!validArgument(rhs, Vector4, "table"))
			throw "parameter 2 has an invalid type '"+type(rhs)+"' ; expected: 'Vector4|table'"
		
		return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z + lhs.w * rhs.w
	}
	
	static function project(vec, onNormal)
	{
		if (!validArgument(vec, Vector4, "table"))
			throw "parameter 1 has an invalid type '"+type(vec)+"' ; expected: 'Vector4|table'"
			
		if (!validArgument(onNormal, Vector4, "table"))
			throw "parameter 2 has an invalid type '"+type(onNormal)+"' ; expected: 'Vector4|table'"
	
		local sqrMagnitude = onNormal.sqrMagnitude()
		
		if (sqrMagnitude < epsilon)
			return Vector4(0, 0, 0, 0)
	
		return onNormal * (dot(vec, onNormal) / sqrMagnitude)
	}
	
	static function lerpUnclamped(a, b, perc)
	{
		if (!validArgument(a, Vector4, "table"))
			throw "parameter 1 has an invalid type '"+type(a)+"' ; expected: 'Vector4|table'"
	
		if (!validArgument(b, Vector4, "table"))
			throw "parameter 2 has an invalid type '"+type(b)+"' ; expected: 'Vector4|table'"
	
		if (!validArgument(perc, "integer", "float"))
			throw "parameter 3 has an invalid type '"+type(perc)+"' ; expected: 'integer|float'"
	
		return Vector4(
			a.x + ((b.x - a.x) * perc), 
			a.y + ((b.y - a.y) * perc), 
			a.z + ((b.z - a.z) * perc),
            a.w + ((b.w - a.w) * perc))
	}
	
	static function lerp(a, b, perc)
	{
		return lerpUnclamped(a, b, clamp(perc, 0, 1))
	}
}

// creating shorthand vector objects

vecZero = Vector4(0, 0, 0, 0)
vecOne = Vector4(1, 1, 1, 1)
