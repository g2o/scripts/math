# Introduction

**math** is a simple squirrel script package mainly made for Gothic 2 Online mod. \
The main idea was to add possibility to manipulate abstract mathematical structures like:
* Vectors
* Matrices
* Quaternions

I've mainly tried to make something similar to Unity math API, also it's worth note, that \
i didn't write every piece of code by my own, if you're interested of the origin of some code, just take a look at **Based on** section.

## Notation

* Script package uses the left hand coordinate system
* Matricies are in row-column order

## How to install?

1.Clone or download the repository source code
2.Extract the code whenether you like in your g2o_server directory
3.Add this line to your XML loading section

```xml
<import src="math/math.xml" />
```

## Based on
* [Unity Math Documentation](https://docs.unity3d.com/Manual/index.html)
* [C# to Lua Repository](https://github.com/topameng/CsToLua)
* [Math for game developers Repository](https://github.com/BSVino/MathForGameDevelopers)
