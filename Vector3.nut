local vecForward = null
local vecUp = null
local vecRight = null

local vecBack = null
local vecDown = null
local vecLeft = null

local vecZero = null
local vecOne = null

// class

class Vector3
{
	static epsilon = 0.000001

	x = 0.0
	y = 0.0
	z = 0.0
	
	constructor (...)
	{
		switch (vargv.len())
		{
			// copy constructor
			
			case 1:
				if (!validArgument(vargv[0], Vector3, "table"))
					throw "parameter 1 has an invalid type '"+type(vargv[0])+"' ; expected: 'Vector3|table'"
			
				x = vargv[0].x
				y = vargv[0].y
				z = vargv[0].z
			
				break
				
			// initial constructor
			
			case 3:			
				if (!validArgument(vargv[0], "integer", "float"))
					throw "parameter 1 has an invalid type '"+type(vargv[0])+"' ; expected: 'integer|float'"
					
				if (!validArgument(vargv[1], "integer", "float"))
					throw "parameter 2 has an invalid type '"+type(vargv[1])+"' ; expected: 'integer|float'"
					
				if (!validArgument(vargv[2], "integer", "float"))
					throw "parameter 3 has an invalid type '"+type(vargv[2])+"' ; expected: 'integer|float'"
					
				x = vargv[0].tofloat()
				y = vargv[1].tofloat()
				z = vargv[2].tofloat()
			
				break
		}
	}
	
	// overloaded operators
	
	function _get(idx)
	{			
		switch (idx)
		{
			case 0: return x
			case 1: return y
			case 2: return z
			
			default: throw null
		}
	}
	
	function _set(idx, value)
	{			
		switch (idx)
		{
			case 0:
				x = value
				break
				
			case 1:
				y = value
				break
				
			case 2:
				z = value
				break
				
			default: 
				throw null
		}
	}
	
	function _add(arg)
	{
		local vec = Vector3(x, y, z)
	
		if (validArgument(arg, Vector3, "table"))
		{
			vec.x += arg.x
			vec.y += arg.y
			vec.z += arg.z
		}
		else
		{
			vec.x += arg
			vec.y += arg
			vec.z += arg
		}
		
		return vec
	}
	
	function _sub(arg)
	{		
		return this + -arg
	}
	
	function _unm()
	{		
		return Vector3(-x, -y, -z)
	}
	
	function _mul(arg)
	{
		local vec = Vector3(x, y, z)
	
		if (validArgument(arg, Vector3, "table"))
		{
			vec.x *= arg.x
			vec.y *= arg.y
			vec.z *= arg.z
		}
		else
		{
			vec.x *= arg
			vec.y *= arg
			vec.z *= arg
		}
		
		return vec
	}
	
	function _div(arg)
	{
		if (validArgument(arg, Vector3, "table"))
			return this * Vector3(1.0 / arg.x, 1.0 / arg.y, 1.0 / arg.z)
		else
			return this * (1.0 / arg)
	}
	
	// methods
	
	function add(arg)
	{
		if (validArgument(arg, Vector3, "table"))
		{
			x += arg.x
			y += arg.y
			z += arg.z
		}
		else
		{
			x += arg
			y += arg
			z += arg
		}
	}
	
	function sub(arg)
	{
		add(-arg)
	}
	
	function mul(arg)
	{
		if (validArgument(arg, Vector3, "table"))
		{
			x *= arg.x
			y *= arg.y
			z *= arg.z
		}
		else
		{
			x *= arg
			y *= arg
			z *= arg
		}
	}
	
	function div(arg)
	{
		if (validArgument(arg, Vector3, "table"))
		{
			x /= arg.x
			y /= arg.y
			z /= arg.z
		}
		else
		{
			x /= arg
			y /= arg
			z /= arg
		}
	}
	
	function set(x, y, z)
	{
		this.x = x
		this.y = y
		this.z = z
	}
	
	function equals(vec)
	{
		if (!validArgument(vec, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(vec)+"' ; expected: 'Vector3|table'"
	
		local diff_x = x - vec.x
        local diff_y = y - vec.y
        local diff_z = z - vec.z

        local sqrMagnitude = diff_x * diff_x + diff_y * diff_y + diff_z * diff_z

        return sqrMagnitude < epsilon * epsilon
	}
	
	function sqrLength()
	{
		return (x * x + y * y + z * z)
	}
	
	function length()
	{
		return sqrt(sqrLength())
	}
	
	function sqrMagnitude()
	{
		return sqrLength()
	}
	
	function magnitude()
	{
		return length()
	}
	
	function normalized()
	{
		local vec = Vector3(this)
		local magnitude = magnitude()

		if (magnitude != 0)
			vec.mul(1.0 / magnitude)
		else
			vec.x = vec.y = vec.z = 0
		
		return vec
	}
	
	function tostring(pattern = "Vector3(%f, %f, %f)")
	{
		return format(pattern, x, y, z)
	}
	
	// static methods
	
	static function forward() { return vecForward }
	static function up() { return vecUp }
	static function right() { return vecRight }

	static function back() { return vecBack }
	static function down() { return vecDown }
	static function left() { return vecLeft}

	static function zero() { return vecZero }
	static function one() { return vecOne }
	
	static function clampMagnitude(vec, maxLength, makeCopy = true)
	{
		if (!validArgument(vec, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(vec)+"' ; expected: 'Vector3|table'"
	
		if (!validArgument(vec, "integer", "float"))
			throw "parameter 2 has an invalid type '"+type(maxLength)+"' ; expected: 'integer|float'"
		
		local result = (makeCopy) ? Vector3(vec) : vec
		
		if (result.sqrMagnitude() > maxLength * maxLength)
		{
			normalize(result)
			result.mul(maxLength)
		}
		
		return result
	}
	
	static function min(lhs, rhs)
	{
		if (!validArgument(lhs, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(lhs)+"' ; expected: 'Vector3|table'"
			
		if (!validArgument(rhs, Vector3, "table"))
			throw "parameter 2 has an invalid type '"+type(rhs)+"' ; expected: 'Vector3|table'"
		
		return Vector3(min(lhs.x, rhs.x), min(lhs.y, rhs.y), min(lhs.z, rhs.z))
	}
	
	static function max(lhs, rhs)
	{
		if (!validArgument(lhs, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(lhs)+"' ; expected: 'Vector3|table'"
			
		if (!validArgument(rhs, Vector3, "table"))
			throw "parameter 2 has an invalid type '"+type(rhs)+"' ; expected: 'Vector3|table'"
			
		return Vector3(max(lhs.x, rhs.x), max(lhs.y, rhs.y), max(lhs.z, rhs.z))
	}
	
	static function distance(a, b)
	{
		if (!validArgument(a, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(a)+"' ; expected: 'Vector3|table'"
			
		if (!validArgument(b, Vector3, "table"))
			throw "parameter 2 has an invalid type '"+type(b)+"' ; expected: 'Vector3|table'"
	
		return (a - b).magnitude()
	}
	
	static function scale(a, b)
	{
		if (!validArgument(a, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(a)+"' ; expected: 'Vector3|table'"
			
		if (!validArgument(b, Vector3, "table"))
			throw "parameter 2 has an invalid type '"+type(b)+"' ; expected: 'Vector3|table'"
		
		return a * b
	}
	
	static function angle(from, to)
	{
		if (!validArgument(from, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(from)+"' ; expected: 'Vector3|table'"
			
		if (!validArgument(to, Vector3, "table"))
			throw "parameter 2 has an invalid type '"+type(to)+"' ; expected: 'Vector3|table'"
		
		return atan2(cross(from, to).magnitude(), dot(from, to)) * rad2Deg
	}
	
	static function signedAngle(from, to, axis)
	{
		if (!validArgument(from, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(from)+"' ; expected: 'Vector3|table'"
			
		if (!validArgument(to, Vector3, "table"))
			throw "parameter 2 has an invalid type '"+type(to)+"' ; expected: 'Vector3|table'"
			
		if (!validArgument(axis, Vector3, "table"))
			throw "parameter 3 has an invalid type '"+type(axis)+"' ; expected: 'Vector3|table'"
		
		return angle(from, to) * sign(dot(axis, cross(from, to)))
	}
	
	static function moveTowards(current, target, maxDistanceDelta)
	{
		if (!validArgument(current, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(current)+"' ; expected: 'Vector3|table'"
			
		if (!validArgument(target, Vector3, "table"))
			throw "parameter 2 has an invalid type '"+type(target)+"' ; expected: 'Vector3|table'"

		if (!validArgument(maxDistanceDelta, "integer", "float"))
			throw "parameter 3 has an invalid type '"+type(maxDistanceDelta)+"' ; expected: 'integer|float'"
		
		local distance = target - current
		local magnitude = distance.magnitude()
		
		if (magnitude <= maxDistanceDelta || magnitude == 0)
			return target
		
		return current + (distance / magnitude) * maxDistanceDelta
	}
	
	static function normalize(vec)
	{
		if (!validArgument(vec, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(vec)+"' ; expected: 'Vector3|table'"
	
		local magnitude = vec.magnitude()

		if (magnitude != 0)
			vec.mul(1.0 / magnitude)
		else
			vec.x = vec.y = vec.z = 0
			
		return vec
	}
	
	static function orthoNormalize(normal, tangent, binormal = null)
	{
		if (!validArgument(normal, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(normal)+"' ; expected: 'Vector3|table'"
			
		if (!validArgument(tangent, Vector3, "table"))
			throw "parameter 2 has an invalid type '"+type(tangent)+"' ; expected: 'Vector3|table'"
		
		if (binormal && !validArgument(binormal, Vector3, "table"))
			throw "parameter 3 has an invalid type '"+type(binormal)+"' ; expected: 'Vector3|table'"
		
		normalize(normal)
		
		local tangentOntoNormal = project(tangent, normal)
		
		tangent.sub(tangentOntoNormal)		
		normalize(tangent)
		
		if (binormal)
		{
			local binormalOntoNormal = project(binormal, normal)
			local binormalOntoTangent = project(binormal, tangent)
			
			binormal.sub(binormalOntoNormal)
			binormal.sub(binormalOntoTangent)
			
			normalize(binormal)
		}
	}
	
	static function dot(lhs, rhs)
	{
		if (!validArgument(lhs, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(lhs)+"' ; expected: 'Vector3|table'"
			
		if (!validArgument(rhs, Vector3, "table"))
			throw "parameter 2 has an invalid type '"+type(rhs)+"' ; expected: 'Vector3|table'"
		
		return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z
	}

	static function cross(lhs, rhs)
	{
		if (!validArgument(lhs, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(lhs)+"' ; expected: 'Vector3|table'"
		
		if (!validArgument(rhs, Vector3, "table"))
			throw "parameter 2 has an invalid type '"+type(rhs)+"' ; expected: 'Vector3|table'"
		
		return Vector3(
			lhs.y * rhs.z - lhs.z * rhs.y, 
			lhs.z * rhs.x - lhs.x * rhs.z, 
			lhs.x * rhs.y - lhs.y * rhs.x)
	}
	
	static function project(vec, onNormal)
	{
		if (!validArgument(vec, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(vec)+"' ; expected: 'Vector3|table'"
			
		if (!validArgument(onNormal, Vector3, "table"))
			throw "parameter 2 has an invalid type '"+type(onNormal)+"' ; expected: 'Vector3|table'"
	
		local sqrMagnitude = onNormal.sqrMagnitude()
		
		if (sqrMagnitude < epsilon)
			return Vector3(0, 0, 0)
	
		return onNormal * (dot(vec, onNormal) / sqrMagnitude)
	}
	
	static function projectOnPlane(vec, planeNormal)
	{
		if (!validArgument(vec, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(vec)+"' ; expected: 'Vector3|table'"
			
		if (!validArgument(planeNormal, Vector3, "table"))
			throw "parameter 2 has an invalid type '"+type(planeNormal)+"' ; expected: 'Vector3|table'"
	
		local result = project(vec, planeNormal)
		
		result.mul(-1)
		result.add(vec)
	
		return result
	}
	
	static function reflect(inDirection, inNormal)
	{
		if (!validArgument(inDirection, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(inDirection)+"' ; expected: 'Vector3|table'"
		
		if (!validArgument(inNormal, Vector3, "table"))
			throw "parameter 2 has an invalid type '"+type(inNormal)+"' ; expected: 'Vector3|table'"
		
		return inDirection + inNormal * -2 * dot(inDirection, inNormal)
	}
	
	static function lerpUnclamped(a, b, perc)
	{
		if (!validArgument(a, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(a)+"' ; expected: 'Vector3|table'"
	
		if (!validArgument(b, Vector3, "table"))
			throw "parameter 2 has an invalid type '"+type(b)+"' ; expected: 'Vector3|table'"
	
		if (!validArgument(perc, "integer", "float"))
			throw "parameter 3 has an invalid type '"+type(perc)+"' ; expected: 'integer|float'"
	
		return Vector3(
			a.x + ((b.x - a.x) * perc), 
			a.y + ((b.y - a.y) * perc), 
			a.z + ((b.z - a.z) * perc))
	}
	
	static function lerp(a, b, perc)
	{
		return lerpUnclamped(a, b, clamp(perc, 0, 1))
	}
		
	static function slerpUnclamped(from, to, t)
	{	
		local v1 = Vector3(from)
		local v2 = Vector3(to)
		
		local len1 = from.magnitude()	
		local len2 = to.magnitude()
		
		v1.div(len1)
		v2.div(len2)
		
		local len = (len2 - len1) * t + len1
		local omega = dot(v1, v2)
		
		local scale0, scale1
		
		if (1 - omega > 1e-6)
		{
			local cosom = acos(omega)
			local sinom = sin(cosom)
			scale0 = sin((1 - t) * cosom) / sinom
			scale1 = sin(t * cosom) / sinom
		}
		else 
		{
			scale0 = 1 - t
			scale1 = t
		}

		v1.mul(scale0)
		v2.mul(scale1)
		
		v2.add(v1)
		v2.mul(len)
		
		return v2
	}
	
	static function slerp(from, to, t)
	{
		return slerpUnclamped(from, to, clamp(t, 0, 1))
	}
}

// creating shorthand vector objects

vecForward = Vector3(0, 0, 1)
vecUp = Vector3(0, 1, 0)
vecRight = Vector3(1, 0, 0)

vecBack = Vector3(0, 0, -1)
vecDown = Vector3(0, -1, 0)
vecLeft = Vector3(-1, 0, 0)

vecZero = Vector3(0, 0, 0)
vecOne = Vector3(1, 1, 1)
